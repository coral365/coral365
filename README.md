# Coral365
Community built Coral Observation Infrastructure

## Problem To Solve

## Solution Design

## Teams

## Cost

## Development Stages

## High Level Schedule

## National Geographic Grants Submission
NatGeo grants money to complete projects with focus in conservation, education, research, storytelling and technology if aligned with wildlife, human journey or changing the planet.

The bleach camera consists of a shared technology that can be easily constructed by locals and acquires coral data to a central location. Members of the diving community around the globe can contribute to provide insight to scientists even when travelling is limited at the moment and foreseeable future.

There are two categories of grants: Exploration Grants and Early Career Grant.
The former requires the applicant to be experienced and have completed tangible similar projects in the past. The latter is intended to give less experienced individuals an opportunity, anyone with at least 5 years of experience in their field may apply.

We align with an Early Career Grant. The eligibility requirements are:
 - Project lasts one year or less;
 - Start date six months after submission deadline (April 21, 2021)
 - One proposal at a time;
 - Submission should be made by the project manager;
 - At least 18 years old;
 - Application in English;
 - Local collaborators;

Started application at: https://grants.nationalgeographic.org.
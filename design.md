# High Level Design of the Coral365 Camera

## True Color Monitoring

### Accuracy Needed
Initial assessment of the accuracy needed was estimated by the work of Siedback et al., who developed a color card with 6 point brightness/saturation scale. A change of 2 units in the scale indicate a bleaching state of the coral.

There are several effects underwater that canb